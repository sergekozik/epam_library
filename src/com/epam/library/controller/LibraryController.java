package com.epam.library.controller;

import com.epam.library.controller.command.ServiceCommand;
import com.epam.library.controller.command.exception.ControllerException;
import com.epam.library.controller.dispatcher.RequestBeanDispatcher;
import com.epam.library.controller.dispatcher.impl.RequestBeanDispatcherImpl;
import com.epam.library.entity.ViewRequestResponseBean;

/**
 * Created by Serge_Kozik on 3/30/2017.
 */
public class LibraryController {

    private final static RequestBeanDispatcher requestDispatcher = RequestBeanDispatcherImpl.getINSTANCE();

    public ViewRequestResponseBean service(ViewRequestResponseBean request) {
        ServiceCommand serviceCommand = requestDispatcher.dispatch(request);
        ViewRequestResponseBean response = null;
        try {
            response = serviceCommand.execute(request);
        } catch (ControllerException e) {
            response = requestDispatcher.dispatchException(e);
        }
        return response;
    }
}
