package com.epam.library.controller.dispatcher;

import com.epam.library.controller.command.ServiceCommand;
import com.epam.library.entity.ViewRequestResponseBean;

/**
 * Created by Serge_Kozik on 3/30/2017.
 */
public interface RequestBeanDispatcher {
    public ServiceCommand dispatch(ViewRequestResponseBean request);
    public ViewRequestResponseBean dispatchException(Exception e);
}
