package com.epam.library.controller.dispatcher.impl;

import com.epam.library.controller.command.ServiceCommand;
import com.epam.library.controller.command.impl.AddRandomBooks;
import com.epam.library.controller.command.impl.DefaultCommand;
import com.epam.library.controller.dispatcher.RequestBeanDispatcher;
import com.epam.library.entity.ViewRequestResponseBean;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Serge_Kozik on 3/30/2017.
 */
public class RequestBeanDispatcherImpl implements RequestBeanDispatcher {

    private final static RequestBeanDispatcher INSTANCE = new RequestBeanDispatcherImpl();
    private final static String DEFAULT_MESSAGE="Unknown Exception.";
    private final static String ERROR_MESSAGE_VARIABLE = "error_message";
    private Map<String,ServiceCommand> linkCommandMap;

    private RequestBeanDispatcherImpl() {
        linkCommandMap = new HashMap();
        linkCommandMap.put("addRandomBooks",new AddRandomBooks());
    }

    @Override
    public ServiceCommand dispatch(ViewRequestResponseBean request) {
        String linkName = request.getLinkName();
        ServiceCommand result = linkCommandMap.get(linkName);
        if (result==null) {
            result = new DefaultCommand();
        }
        return result;
    }

    @Override
    public ViewRequestResponseBean dispatchException(Exception e) {
        ViewRequestResponseBean result = new ViewRequestResponseBean("exception");
        String message;
        if (e == null) {
            message = DEFAULT_MESSAGE;
        } else {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            message = sw.toString();
        }
        result.setAttribute(ERROR_MESSAGE_VARIABLE,message);
        return result;
    }

    public static RequestBeanDispatcher getINSTANCE() {
        return INSTANCE;
    }
}
