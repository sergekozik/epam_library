package com.epam.library.controller.command;

import com.epam.library.controller.command.exception.ControllerException;
import com.epam.library.entity.ViewRequestResponseBean;

/**
 * Created by Serge_Kozik on 3/30/2017.
 */
public interface ServiceCommand {
    public ViewRequestResponseBean execute(ViewRequestResponseBean request) throws ControllerException;
}
