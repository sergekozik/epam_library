package com.epam.library.controller.command.impl;

import com.epam.library.controller.command.ServiceCommand;
import com.epam.library.controller.command.exception.ControllerException;
import com.epam.library.entity.ViewRequestResponseBean;
import com.epam.library.service.BookService;
import com.epam.library.service.exception.ServiceException;
import com.epam.library.service.impl.BookServiceImpl;

/**
 * Created by Serge_Kozik on 3/30/2017.
 */
public class AddRandomBooks implements ServiceCommand {

    private final static String BOOKS_NUMBER_VARIABLE = "books_number";
    private final static String MESSAGE_VARIABLE = "message_report";
    private final static String REPORT_SUCCESS = "Books were added.";
    private final static String REPORT_FAIL = "Books were not added.";
    private final static String LINK_FOR_VIEW = "standard-report";
    private final static BookService bookService = BookServiceImpl.getInstance();

    @Override
    public ViewRequestResponseBean execute(ViewRequestResponseBean request) throws ControllerException {
        Object booksNumObj = request.getAttribute(BOOKS_NUMBER_VARIABLE);
        if (booksNumObj==null) {
            throw new ControllerException("Books number is undefined.");
        }
        int booksNum=0;
        boolean resultAction = false;
        try {
            String booksNumString = (String)booksNumObj;
            booksNum=Integer.parseInt(booksNumString);
            resultAction = bookService.generateBooks(booksNum);
        } catch (NumberFormatException e) {
            throw new ControllerException(e);
        } catch (ServiceException e2) {
            throw new ControllerException(e2);
        }
        ViewRequestResponseBean response = new ViewRequestResponseBean(LINK_FOR_VIEW);
        String report = null;
        if (resultAction) {
            report = REPORT_SUCCESS;
        } else {
            report = REPORT_FAIL;
        }
        response.setAttribute(MESSAGE_VARIABLE,report);
        return response;
    }
}
