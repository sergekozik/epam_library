package com.epam.library.setting;

/**
 * Created by Serge on 18.03.2017.
 */
public final class DatabaseConstants {
    public static final String DB_URL = "jdbc:mysql://localhost:3306/epam_library";
    public static final String DB_LOGIN = "root";
    public static final String DB_PASSWORD = "1234";
}
