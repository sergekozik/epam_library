package com.epam.library.entity;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Serge_Kozik on 3/29/2017.
 */
public class ViewRequestResponseBean implements Serializable {
    private static final long serialVersionUID = 1;
    private Map<String,Object> attributesMap;
    private String linkName;

    public ViewRequestResponseBean() {
        attributesMap = new HashMap<>();
    }

    public ViewRequestResponseBean(String linkName) {
        this.linkName = linkName;
    }

    public Object getAttribute(String attrName) {
        return attributesMap.get(attrName);
    }

    public void setAttribute(String attrName, Object attrObject) {
        attributesMap.put(attrName,attrObject);
    }

    public String getLinkName() {
        return linkName;
    }

    public void setLinkName(String linkName) {
        this.linkName = linkName;
    }
}
