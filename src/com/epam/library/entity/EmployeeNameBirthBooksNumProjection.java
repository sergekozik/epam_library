package com.epam.library.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Serge on 18.03.2017.
 */
public class EmployeeNameBirthBooksNumProjection implements Serializable {

    private String name;
    private int booksNumber;
    private Date birthDate;

    public EmployeeNameBirthBooksNumProjection() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getBooksNumber() {
        return booksNumber;
    }

    public void setBooksNumber(int booksNumber) {
        this.booksNumber = booksNumber;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }
}
