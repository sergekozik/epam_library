package com.epam.library.entity;

import java.io.Serializable;

/**
 * Created by Serge on 18.03.2017.
 */
public class EmployeeNameBooksNumProjection implements Serializable {
    private String name;
    private int booksNumber;

    public EmployeeNameBooksNumProjection() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getBooksNumber() {
        return booksNumber;
    }

    public void setBooksNumber(int booksNumber) {
        this.booksNumber = booksNumber;
    }
}
