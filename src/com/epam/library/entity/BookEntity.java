package com.epam.library.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Serge on 18.03.2017.
 */
public class BookEntity implements Serializable {
    private int id;
    private String title;
    private String brief;
    private int publishYear;
    private String author;
    private List<Integer> employeesId;

    public BookEntity() {
        employeesId = new ArrayList<>();
    }

    public BookEntity(String title, String brief, int publishYear, String author) {
        this.title = title;
        this.brief = brief;
        this.publishYear = publishYear;
        this.author = author;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBrief() {
        return brief;
    }

    public void setBrief(String brief) {
        this.brief = brief;
    }

    public int getPublishYear() {
        return publishYear;
    }

    public void setPublishYear(int publishYear) {
        this.publishYear = publishYear;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public List<Integer> getEmployeesId() {
        return employeesId;
    }

    public void setEmployeesId(List<Integer> employeesId) {
        this.employeesId = employeesId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BookEntity that = (BookEntity) o;

        if (getId() != that.getId()) return false;
        if (getPublishYear() != that.getPublishYear()) return false;
        if (getTitle() != null ? !getTitle().equals(that.getTitle()) : that.getTitle() != null) return false;
        if (getBrief() != null ? !getBrief().equals(that.getBrief()) : that.getBrief() != null) return false;
        if (getAuthor() != null ? !getAuthor().equals(that.getAuthor()) : that.getAuthor() != null) return false;
        return getEmployeesId() != null ? getEmployeesId().equals(that.getEmployeesId()) : that.getEmployeesId() == null;

    }

    @Override
    public int hashCode() {
        int result = getId();
        result = 31 * result + (getTitle() != null ? getTitle().hashCode() : 0);
        result = 31 * result + (getBrief() != null ? getBrief().hashCode() : 0);
        result = 31 * result + getPublishYear();
        result = 31 * result + (getAuthor() != null ? getAuthor().hashCode() : 0);
        result = 31 * result + (getEmployeesId() != null ? getEmployeesId().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "BookEntity{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", brief='" + brief + '\'' +
                ", publishYear=" + publishYear +
                ", author='" + author + '\'' +
                '}';
    }
}
