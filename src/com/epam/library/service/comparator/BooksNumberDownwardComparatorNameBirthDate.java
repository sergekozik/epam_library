package com.epam.library.service.comparator;

import com.epam.library.entity.EmployeeNameBirthBooksNumProjection;

import java.text.Collator;
import java.util.Comparator;
import java.util.Locale;

/**
 * Created by Serge on 20.03.2017.
 */
public class BooksNumberDownwardComparatorNameBirthDate implements Comparator<EmployeeNameBirthBooksNumProjection> {

    @Override
    public int compare(EmployeeNameBirthBooksNumProjection o1, EmployeeNameBirthBooksNumProjection o2) {
        if (o1==null) {
            if (o2==null) {
                return 0;
            }
            return -1;
        }
        if (o2==null) {
            return 1;
        }
        if (o2.getBirthDate().equals(o1.getBirthDate())) {
            if (o1.getBooksNumber()==o2.getBooksNumber()) {
                Collator collator = Collator.getInstance(Locale.getDefault());
                return collator.compare(o1.getName(),o2.getName());
            }
            return o2.getBooksNumber()-o1.getBooksNumber();
        }
        return o1.getBirthDate().compareTo(o2.getBirthDate());
    }
}
