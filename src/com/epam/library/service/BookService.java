package com.epam.library.service;

import com.epam.library.service.exception.ServiceException;

/**
 * Created by Serge on 19.03.2017.
 */
public interface BookService {
    public boolean generateBooks(int number) throws ServiceException;
}
