package com.epam.library.service.impl;

import com.epam.library.dao.EmployeeDao;
import com.epam.library.dao.impl.EmployeeDaoJdbcImpl;
import com.epam.library.entity.EmployeeNameBirthBooksNumProjection;
import com.epam.library.entity.EmployeeNameBooksNumProjection;
import com.epam.library.dao.exception.DaoException;
import com.epam.library.service.EmployeeService;
import com.epam.library.service.exception.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

/**
 * Created by Serge on 19.03.2017.
 */
public class EmployeeServiceImpl implements EmployeeService {

    private static EmployeeService instance = new EmployeeServiceImpl();
    private static EmployeeDao employeeDao = EmployeeDaoJdbcImpl.getInstance();
    private static Logger logger = LogManager.getLogger();

    private EmployeeServiceImpl() {
    }

    public static EmployeeService getInstance() {
        return instance;
    }

    @Override
    public boolean generateRead(int number, int maxBookId, int maxEmployeeId) throws ServiceException {
        Random generator = new Random();
        int numSaved = 0;
        while (numSaved < number) {
            int bookId = generator.nextInt(maxBookId);
            int employeeId = generator.nextInt(maxEmployeeId);
            try {
                if (employeeDao.markBookReadByEmployee(bookId, employeeId)) {
                    numSaved++;
                }
            } catch (DaoException e) {
                logger.error(e.getMessage());
                throw new ServiceException(e);
            }
        }
        return (numSaved>0);
    }

    @Override
    public List<EmployeeNameBooksNumProjection> showAllWhoReadMoreThan(int booksNumber, Comparator<EmployeeNameBooksNumProjection> comparator) throws ServiceException {
        List<EmployeeNameBooksNumProjection> result = new ArrayList<>();
        try {
            List<EmployeeNameBooksNumProjection> all = employeeDao.findAllBooksNumberProjectedByName();
            //replace following by JDK8 stream or move this logic to SQL
            for (EmployeeNameBooksNumProjection projection:all) {
                if (projection.getBooksNumber()>=booksNumber) {
                    result.add(projection);
                }
            }
            if (comparator!=null) {
                result.sort(comparator);
            }
        } catch (DaoException e) {
            logger.error(e.getMessage());
            throw new ServiceException(e);
        }
        return result;
    }

    @Override
    public List<EmployeeNameBirthBooksNumProjection> showAllWhoReadUpTo(int booksNumber, Comparator<EmployeeNameBirthBooksNumProjection> comparator) throws ServiceException {
        List<EmployeeNameBirthBooksNumProjection> result = new ArrayList<>();
        try {
            List<EmployeeNameBirthBooksNumProjection> all = employeeDao.findAllBooksNumberProjectedByNameBirth();
            //replace following by JDK8 stream or move this logic to SQL
            for (EmployeeNameBirthBooksNumProjection projection:all) {
                if (projection.getBooksNumber()<=booksNumber) {
                    result.add(projection);
                }
            }
            if (comparator!=null) {
                result.sort(comparator);
            }
        } catch (DaoException e) {
            logger.error(e.getMessage());
            throw new ServiceException(e);
        }
        return result;
    }
}
