package com.epam.library.service.impl;

import com.epam.library.dao.BookDao;
import com.epam.library.dao.impl.BookDaoJdbcImpl;
import com.epam.library.entity.BookEntity;
import com.epam.library.dao.exception.DaoException;
import com.epam.library.service.BookService;
import com.epam.library.service.exception.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Serge on 19.03.2017.
 */
public class BookServiceImpl implements BookService {

    private final static BookService INSTANCE = new BookServiceImpl();
    private final static int DEFAULT_TITLE_LENGTH =10;
    private final static int DEFAULT_AUTHOR_LENGTH =3;
    private final static int MAXIMUM_RANDOM_YEAR =2017;

    private static BookDao bookDao = BookDaoJdbcImpl.getInstance();
    private static Logger logger = LogManager.getLogger();


    private BookServiceImpl() {
    }

    public static BookService getInstance() {
        return INSTANCE;
    }

    @Override
    public boolean generateBooks(int number) throws ServiceException {
        List<String> resultTitles = new ArrayList<>();
        Random generator = new Random();
        int savedBooks=0;
        while (savedBooks<number) {
            String title = generateRandomString(generator,DEFAULT_TITLE_LENGTH);
            if (resultTitles.contains(title)) {
                continue;
            }
            resultTitles.add(title);
            String author = generateRandomString(generator,DEFAULT_AUTHOR_LENGTH);
            int publishYear = generator.nextInt(MAXIMUM_RANDOM_YEAR);
            BookEntity entity = new BookEntity(title,"",publishYear,author);
            try{
                BookEntity savedEntity = bookDao.save(entity);
                if (savedEntity!=null) {
                    savedBooks++;
                }
            } catch (DaoException e) {
                logger.error(e.getMessage());
                throw new ServiceException(e);
            }
        }
        return (savedBooks>0);
    }

    private String generateRandomString(Random generator, int stringLength) {
        /*StringBuilder sb = new StringBuilder();
        for(int i = 0; i < stringLength; i++) {
            char c = (char)(generator.nextInt((int)(Character.MAX_VALUE)));
            sb.append(c);
        }
        return sb.toString();*/
        char[] chars = "abcdefghijklmnopqrstuvwxyz".toCharArray();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < stringLength; i++) {
            char c = chars[generator.nextInt(chars.length)];
            sb.append(c);
        }
        return sb.toString();
    }
}
