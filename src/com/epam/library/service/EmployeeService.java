package com.epam.library.service;

import com.epam.library.entity.EmployeeNameBirthBooksNumProjection;
import com.epam.library.entity.EmployeeNameBooksNumProjection;
import com.epam.library.service.exception.ServiceException;

import java.util.Comparator;
import java.util.List;

/**
 * Created by Serge on 19.03.2017.
 */
public interface EmployeeService {
    public boolean generateRead(int number, int maxBookId, int maxEmployeeId) throws ServiceException;
    public List<EmployeeNameBooksNumProjection> showAllWhoReadMoreThan(int booksNumber, Comparator<EmployeeNameBooksNumProjection> comparator) throws ServiceException;
    public List<EmployeeNameBirthBooksNumProjection> showAllWhoReadUpTo(int booksNumber, Comparator<EmployeeNameBirthBooksNumProjection> comparator) throws ServiceException;
}
