package com.epam.library.main;

import com.epam.library.entity.EmployeeNameBirthBooksNumProjection;
import com.epam.library.entity.EmployeeNameBooksNumProjection;
import com.epam.library.service.BookService;
import com.epam.library.service.EmployeeService;
import com.epam.library.service.comparator.BooksNumberComparatorName;
import com.epam.library.service.comparator.BooksNumberDownwardComparatorNameBirthDate;
import com.epam.library.service.impl.BookServiceImpl;
import com.epam.library.service.impl.EmployeeServiceImpl;
import com.epam.library.view.console_printer.CustomConsolePrinter;
import com.epam.library.view.console_printer.impl.CustomConsolePrinterImpl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

/**
 * Created by Serge on 18.03.2017.
 */
public class Main {

    private static BookService bookService = BookServiceImpl.getInstance();
    private static EmployeeService employeeService = EmployeeServiceImpl.getInstance();
    private static CustomConsolePrinter customConsolePrinter = CustomConsolePrinterImpl.getInstance();

    public static void main(String[] args) {
        //bookService.generateBooks(50);
        //employeeService.generateRead(200,50,13);
        goodReadersReport(18);
        badReadersReport(18);
    }

    public static void goodReadersReport (int booksNumber) {
        List<EmployeeNameBooksNumProjection> readers = employeeService.showAllWhoReadMoreThan(booksNumber, new BooksNumberComparatorName());
        if (readers!=null) {
            List<String> firstColumn = new ArrayList<>(readers.size());
            List<String> secondColumn = new ArrayList<>(readers.size());
            for (EmployeeNameBooksNumProjection reader:readers) {
                firstColumn.add(reader.getName());
                secondColumn.add(String.valueOf(reader.getBooksNumber()));
            }
            customConsolePrinter.printLine("Readers, who read equal or more than "+String.valueOf(booksNumber)+" books:");
            customConsolePrinter.printTwoColumns(firstColumn,secondColumn);
        }
    }

    public static void badReadersReport (int booksNumber) {
        List<EmployeeNameBirthBooksNumProjection> readers = employeeService.showAllWhoReadUpTo(booksNumber,new BooksNumberDownwardComparatorNameBirthDate());
        if (readers!=null) {
            List<String> firstColumn = new ArrayList<>(readers.size());
            List<String> secondColumn = new ArrayList<>(readers.size());
            List<String> thirdColumn = new ArrayList<>(readers.size());
            DateFormat dateFormat = new SimpleDateFormat("dd.MM.YYYY");
            dateFormat.setTimeZone(TimeZone.getDefault());
            for (EmployeeNameBirthBooksNumProjection reader:readers) {
                firstColumn.add(reader.getName());
                secondColumn.add(dateFormat.format(reader.getBirthDate()));
                thirdColumn.add(String.valueOf(reader.getBooksNumber()));
            }
            customConsolePrinter.printLine("Readers, who read equal or less than "+String.valueOf(booksNumber)+" books:");
            customConsolePrinter.printThreeColumns(firstColumn,secondColumn,thirdColumn);
        }
    }
}
