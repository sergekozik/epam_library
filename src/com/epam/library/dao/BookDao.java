package com.epam.library.dao;

import com.epam.library.entity.BookEntity;
import com.epam.library.dao.exception.DaoException;

import java.util.List;

/**
 * Created by Serge on 18.03.2017.
 */
public interface BookDao {

    public BookEntity save(BookEntity entity) throws DaoException;
    public BookEntity findOne(int entityId) throws DaoException;
    public boolean delete(int entityId) throws DaoException;
    public List<BookEntity> findByTitle(String title) throws DaoException;
}
