package com.epam.library.dao;

import com.epam.library.entity.EmployeeEntity;
import com.epam.library.entity.EmployeeNameBirthBooksNumProjection;
import com.epam.library.entity.EmployeeNameBooksNumProjection;
import com.epam.library.dao.exception.DaoException;

import java.util.List;

/**
 * Created by Serge on 18.03.2017.
 */
public interface EmployeeDao {
    public EmployeeEntity save(EmployeeEntity entity) throws DaoException;
    public boolean delete(int entityId) throws DaoException;
    public List<EmployeeNameBooksNumProjection> findAllBooksNumberProjectedByName() throws DaoException;
    public List<EmployeeNameBirthBooksNumProjection> findAllBooksNumberProjectedByNameBirth() throws DaoException;
    public boolean markBookReadByEmployee(int bookId, int employeeId) throws DaoException;
}
