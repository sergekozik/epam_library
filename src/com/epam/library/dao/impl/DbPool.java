package com.epam.library.dao.impl;

import com.epam.library.setting.DatabaseConstants;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;

/**
 * Created by Serge on 18.03.2017.
 */
public class DbPool {
    private static Driver sqlDriver = null;
    private static Logger logger = LogManager.getLogger();
    private static DbPool instance = new DbPool();

    private DbPool() {
        try {
            sqlDriver = new com.mysql.jdbc.Driver();
            DriverManager.registerDriver(sqlDriver);
        } catch (SQLException e) {
            logger.error("MySQL Driver manager is not initialized. DbPool. "+e.getMessage());
            throw new RuntimeException();
        }
    }

    public static DbPool getInstance() {
        return instance;
    }

    public Connection getConnection() throws SQLException {
        return DriverManager.getConnection(DatabaseConstants.DB_URL,DatabaseConstants.DB_LOGIN,DatabaseConstants.DB_PASSWORD);
    }

    public void closeDbResources(Connection connection) {
        closeConnection(connection);
    }

    public void closeDbResources(Connection connection, PreparedStatement statement) {
        closePreparedStatement(statement);
        closeConnection(connection);
    }

    public void closeDbResources(Connection connection, PreparedStatement statement, ResultSet resultSet) {
        closeResultSet(resultSet);
        closePreparedStatement(statement);
        closeConnection(connection);
    }

    private void closeResultSet( ResultSet resultSet) {
        if (resultSet!=null) {
            try {
                resultSet.close();
            }catch (SQLException e) {
                logger.error("Error while ResultSet closing."+e.getMessage());
            }
        }
    }

    private void closeConnection( Connection connection) {
        if (connection!=null) {
            try {
                connection.close();
            }catch (SQLException e) {
                logger.error("Error while Connection closing."+e.getMessage());
            }
        }
    }

    private void closePreparedStatement( PreparedStatement statement) {
        if (statement!=null) {
            try {
                statement.close();
            }catch (SQLException e) {
                logger.error("Error while PreparedStatement closing."+e.getMessage());
            }
        }
    }
}
