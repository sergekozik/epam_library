package com.epam.library.dao.impl;

import com.epam.library.dao.BookDao;
import com.epam.library.entity.BookEntity;
import com.epam.library.dao.exception.DaoException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Serge on 18.03.2017.
 */
public class BookDaoJdbcImpl implements BookDao {

    private static final String SQL_INSERT="INSERT INTO book(TITLE,BRIEF,AUTHOR,PUBLISH_YEAR) VALUES(?,?,?,?)";
    private static final String SQL_UPDATE="UPDATE book SET TITLE=?, BRIEF=?, AUTHOR=?, PUBLISH_YEAR=? WHERE ID=?";
    private static final String SQL_SELECT_ID="SELECT ID FROM book WHERE TITLE=? AND BRIEF=? AND AUTHOR=? AND PUBLISH_YEAR=?";
    private static final String SQL_CHECK_ID="SELECT ID FROM book WHERE ID=?";
    private static final String SQL_SELECT="SELECT ID,TITLE,BRIEF,AUTHOR,PUBLISH_YEAR FROM book WHERE ID=?";
    private static final String SQL_SELECT_BY_TITLE="SELECT ID,TITLE,BRIEF,AUTHOR,PUBLISH_YEAR FROM book WHERE TITLE=?";
    private static final String SQL_SELECT_ALL_EMPLOYEES="SELECT EMPLOYEE_ID FROM employee_book WHERE BOOK_ID=?";
    private static final String SQL_BOOK_DELETE = "DELETE FROM book WHERE ID=%d";
    private static final String SQL_JOIN_TABLE_DELETE = "DELETE FROM employee_book WHERE BOOK_ID=%d";

    private static BookDao instance = new BookDaoJdbcImpl();

    private BookDaoJdbcImpl() {

    }

    public static BookDao getInstance() {
        return instance;
    }

    @Override
    public BookEntity save(BookEntity entity) throws DaoException {

        if (entity==null) {
            return null;
        }
        Connection connection = null;
        PreparedStatement statement=null;
        try {
            connection = DbPool.getInstance().getConnection();
            statement = connection.prepareStatement(SQL_SELECT_ID);
            statement.setString(1,entity.getTitle());
            statement.setString(2,entity.getBrief());
            statement.setString(3,entity.getAuthor());
            statement.setInt(4,entity.getPublishYear());
            ResultSet resultSet = statement.executeQuery();
            Integer id = null;
            if (resultSet.next()) {
                id = resultSet.getInt("ID");
            }
            resultSet.close();
            statement.close();
            if (id!=null) {
                statement = connection.prepareStatement(SQL_UPDATE);
                statement.setString(1,entity.getTitle());
                statement.setString(2,entity.getBrief());
                statement.setString(3,entity.getAuthor());
                statement.setInt(4,entity.getPublishYear());
                statement.setInt(5,id);
                statement.executeUpdate();
            } else {
                statement = connection.prepareStatement(SQL_INSERT,PreparedStatement.RETURN_GENERATED_KEYS);
                statement.setString(1,entity.getTitle());
                statement.setString(2,entity.getBrief());
                statement.setString(3,entity.getAuthor());
                statement.setInt(4,entity.getPublishYear());
                statement.executeUpdate();
                ResultSet resultSetId = statement.getGeneratedKeys();
                if (resultSetId.next()) {
                    entity.setId(resultSetId.getInt(1));
                }
            }
            return entity;
        } catch (SQLException e) {
            throw new DaoException("Error while book entity saving. "+e.getMessage());
        } finally {
            DbPool.getInstance().closeDbResources(connection,statement);
        }
    }

    @Override
    public BookEntity findOne(int entityId) throws DaoException {
        Connection connection = null;
        PreparedStatement statement=null;
        BookEntity resultEntity = null;
        try {
            connection = DbPool.getInstance().getConnection();
            statement = connection.prepareStatement(SQL_SELECT);
            statement.setInt(1,entityId);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                resultEntity = new BookEntity();
                resultEntity.setId(resultSet.getInt("ID"));
                resultEntity.setTitle(resultSet.getString("TITLE"));
                resultEntity.setBrief(resultSet.getString("BRIEF"));
                resultEntity.setAuthor(resultSet.getString("AUTHOR"));
                resultEntity.setPublishYear(resultSet.getInt("PUBLISH_YEAR"));
            } else {
                return null;
            }
            resultSet.close();
            statement.close();
            statement = connection.prepareStatement(SQL_SELECT_ALL_EMPLOYEES);
            statement.setInt(1,entityId);
            ResultSet resultSetEmpl = statement.executeQuery();
            while (resultSetEmpl.next()) {
                resultEntity.getEmployeesId().add(new Integer(resultSetEmpl.getInt("EMPLOYEE_ID")));
            }
            return resultEntity;
        } catch (SQLException e) {
            throw new DaoException("Error while book search with id="+ String.valueOf(entityId)+". "+e.getMessage());
        } finally {
            DbPool.getInstance().closeDbResources(connection,statement);
        }
    }

    @Override
    public List<BookEntity> findByTitle(String title) throws DaoException {
        Connection connection = null;
        PreparedStatement statement=null;
        List<BookEntity> resultList = null;
        try {
            connection = DbPool.getInstance().getConnection();
            statement = connection.prepareStatement(SQL_SELECT_BY_TITLE);
            statement.setString(1,title);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.isBeforeFirst()) {
                resultList = new ArrayList<>();
                while (resultSet.next()) {
                    BookEntity resultEntity = new BookEntity();
                    resultEntity.setId(resultSet.getInt("ID"));
                    resultEntity.setTitle(resultSet.getString("TITLE"));
                    resultEntity.setBrief(resultSet.getString("BRIEF"));
                    resultEntity.setAuthor(resultSet.getString("AUTHOR"));
                    resultEntity.setPublishYear(resultSet.getInt("PUBLISH_YEAR"));
                    PreparedStatement statementEmpl = connection.prepareStatement(SQL_SELECT_ALL_EMPLOYEES);
                    statementEmpl.setInt(1,resultEntity.getId());
                    ResultSet resultSetEmpl = statementEmpl.executeQuery();
                    while (resultSetEmpl.next()) {
                        resultEntity.getEmployeesId().add(new Integer(resultSetEmpl.getInt("EMPLOYEE_ID")));
                    }
                    resultList.add(resultEntity);
                    statementEmpl.close();
                }
            }
            return resultList;
        } catch (SQLException e) {
            throw new DaoException("Error while book search with title="+title+". "+e.getMessage());
        } finally {
            DbPool.getInstance().closeDbResources(connection,statement);
        }
    }

    @Override
    public boolean delete(int entityId) throws DaoException {
        Connection connection = null;
        PreparedStatement preparedStatement=null;
        Statement statement = null;
        try {
            connection = DbPool.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(SQL_CHECK_ID);
            preparedStatement.setInt(1,entityId);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                connection.setAutoCommit(false);
                statement = connection.createStatement();
                statement.addBatch("SET FOREIGN_KEY_CHECKS=0");
                statement.addBatch(String.format(SQL_JOIN_TABLE_DELETE,entityId));
                statement.addBatch(String.format(SQL_BOOK_DELETE,entityId));
                statement.addBatch("SET FOREIGN_KEY_CHECKS=1");
                statement.executeBatch();
                connection.commit();
                connection.setAutoCommit(true);
                return true;
            } else {
                return false;
            }
        } catch (SQLException e) {
            String message = "Error while deleting book entity id="+ String.valueOf(entityId)+". "+e.getMessage();
            try{
                if (connection!=null) {
                    connection.rollback();
                }
            } catch (SQLException e2) {
                message = " Error rollback delete Book."+ message+e2.getMessage();
            }
            throw new DaoException(message);
        } finally {
            DbPool.getInstance().closeDbResources(connection);
        }
    }
}
