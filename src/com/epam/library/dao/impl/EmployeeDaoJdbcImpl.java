package com.epam.library.dao.impl;

import com.epam.library.dao.EmployeeDao;
import com.epam.library.entity.EmployeeEntity;
import com.epam.library.entity.EmployeeNameBirthBooksNumProjection;
import com.epam.library.entity.EmployeeNameBooksNumProjection;
import com.epam.library.dao.exception.DaoException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Serge on 19.03.2017.
 */
public class EmployeeDaoJdbcImpl implements EmployeeDao {

    private static final String SQL_SELECT_ID="SELECT ID FROM employee WHERE NAME=? AND EMAIL=? AND DATE_OF_BIRTH=?";
    private static final String SQL_UPDATE="UPDATE employee SET NAME=?, EMAIL=?, DATE_OF_BIRTH=? WHERE ID=?";
    private static final String SQL_INSERT="INSERT INTO employee(NAME,EMAIL,DATE_OF_BIRTH) VALUES(?,?,?)";
    private static final String SQL_CHECK_ID="SELECT ID FROM employee WHERE ID=?";
    private static final String SQL_CHECK_BOOK_ID="SELECT ID FROM book WHERE ID=?";
    private static final String SQL_JOIN_TABLE_DELETE = "DELETE FROM employee_book WHERE EMPLOYEE_ID=%d";
    private static final String SQL_JOIN_TABLE_SELECT = "SELECT BOOK_ID FROM employee_book WHERE EMPLOYEE_ID=?";
    private static final String SQL_JOIN_TABLE_INSERT = "INSERT INTO employee_book(BOOK_ID,EMPLOYEE_ID) VALUES(?,?)";
    private static final String SQL_EMPLOYEE_DELETE = "DELETE FROM employee WHERE ID=%d";
    private static final String SQL_BOOK_COUNT_NAME="SELECT ee.NAME AS username, COUNT(eb.EMPLOYEE_ID) AS bookread FROM employee_book eb RIGHT JOIN employee ee ON eb.EMPLOYEE_ID=ee.ID GROUP BY ee.ID";
    private static final String SQL_BOOK_COUNT_NAME_BIRTHDATE="SELECT ee.NAME AS username, ee.DATE_OF_BIRTH AS birthdate, COUNT(eb.EMPLOYEE_ID) AS bookread FROM employee_book eb RIGHT JOIN employee ee ON eb.EMPLOYEE_ID=ee.ID GROUP BY ee.ID";

    private static EmployeeDaoJdbcImpl instance = new EmployeeDaoJdbcImpl();

    private EmployeeDaoJdbcImpl() {
    }

    public static EmployeeDaoJdbcImpl getInstance() {
        return instance;
    }

    @Override
    public EmployeeEntity save(EmployeeEntity entity) throws DaoException {

        if (entity==null) {
            return null;
        }
        Connection connection = null;
        PreparedStatement statement=null;
        try {
            connection = DbPool.getInstance().getConnection();
            statement = connection.prepareStatement(SQL_SELECT_ID);
            statement.setString(1,entity.getName());
            statement.setString(2,entity.getEmail());
            statement.setDate(3,new java.sql.Date(entity.getDateOfBirth().getTime()));
            ResultSet resultSet = statement.executeQuery();
            Integer id = null;
            if (resultSet.next()) {
                id = resultSet.getInt("ID");
            }
            resultSet.close();
            statement.close();
            if (id!=null) {
                statement = connection.prepareStatement(SQL_UPDATE);
                statement.setString(1,entity.getName());
                statement.setString(2,entity.getEmail());
                statement.setDate(3,new java.sql.Date(entity.getDateOfBirth().getTime()));
                statement.setInt(4,id);
                statement.executeUpdate();
            } else {
                statement = connection.prepareStatement(SQL_INSERT,PreparedStatement.RETURN_GENERATED_KEYS);
                statement.setString(1,entity.getName());
                statement.setString(2,entity.getEmail());
                statement.setDate(3,new java.sql.Date(entity.getDateOfBirth().getTime()));
                statement.executeUpdate();
                ResultSet resultSetId = statement.getGeneratedKeys();
                if (resultSetId.next()) {
                    entity.setId(resultSetId.getInt(1));
                }
            }
            return entity;
        } catch (SQLException e) {
            throw new DaoException("Error while employee entity saving. "+e.getMessage());
        } finally {
            DbPool.getInstance().closeDbResources(connection,statement);
        }
    }

    @Override
    public boolean delete(int entityId) throws DaoException {
        Connection connection = null;
        PreparedStatement preparedStatement=null;
        Statement statement = null;
        try {
            connection = DbPool.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(SQL_CHECK_ID);
            preparedStatement.setInt(1,entityId);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                connection.setAutoCommit(false);
                statement = connection.createStatement();
                statement.addBatch("SET FOREIGN_KEY_CHECKS=0");
                statement.addBatch(String.format(SQL_JOIN_TABLE_DELETE,entityId));
                statement.addBatch(String.format(SQL_EMPLOYEE_DELETE,entityId));
                statement.addBatch("SET FOREIGN_KEY_CHECKS=1");
                statement.executeBatch();
                connection.commit();
                connection.setAutoCommit(true);
                return true;
            } else {
                return false;
            }
        } catch (SQLException e) {
            String message = "Error while deleting employee entity id="+ String.valueOf(entityId)+". "+e.getMessage();
            try{
                if (connection!=null) {
                    connection.rollback();
                }
            } catch (SQLException e2) {
                message = " Error rollback delete employee."+ message+e2.getMessage();
            }
            throw new DaoException(message);
        } finally {
            DbPool.getInstance().closeDbResources(connection);
        }
    }

    @Override
    public List<EmployeeNameBooksNumProjection> findAllBooksNumberProjectedByName() throws DaoException {
        Connection connection = null;
        PreparedStatement statement=null;
        List<EmployeeNameBooksNumProjection> resultList = null;
        try {
            connection = DbPool.getInstance().getConnection();
            statement = connection.prepareStatement(SQL_BOOK_COUNT_NAME);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.isBeforeFirst()) {
                resultList = new ArrayList<>();
                while (resultSet.next()) {
                    EmployeeNameBooksNumProjection resultEntity = new EmployeeNameBooksNumProjection();
                    resultEntity.setName(resultSet.getString("username"));
                    resultEntity.setBooksNumber(resultSet.getInt("bookread"));
                    resultList.add(resultEntity);
                }
            }
            return resultList;
        } catch (SQLException e) {
            throw new DaoException("Error while counting book per employee"+e.getMessage());
        } finally {
            DbPool.getInstance().closeDbResources(connection,statement);
        }
    }

    @Override
    public List<EmployeeNameBirthBooksNumProjection> findAllBooksNumberProjectedByNameBirth() throws DaoException {
        Connection connection = null;
        PreparedStatement statement=null;
        List<EmployeeNameBirthBooksNumProjection> resultList = null;
        try {
            connection = DbPool.getInstance().getConnection();
            statement = connection.prepareStatement(SQL_BOOK_COUNT_NAME_BIRTHDATE);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.isBeforeFirst()) {
                resultList = new ArrayList<>();
                while (resultSet.next()) {
                    EmployeeNameBirthBooksNumProjection resultEntity = new EmployeeNameBirthBooksNumProjection();
                    resultEntity.setName(resultSet.getString("username"));
                    resultEntity.setBooksNumber(resultSet.getInt("bookread"));
                    resultEntity.setBirthDate(new java.util.Date(resultSet.getDate("birthdate").getTime()));
                    resultList.add(resultEntity);
                }
            }
            return resultList;
        } catch (SQLException e) {
            throw new DaoException("Error while counting book per employee"+e.getMessage());
        } finally {
            DbPool.getInstance().closeDbResources(connection,statement);
        }
    }

    @Override
    public boolean markBookReadByEmployee(int bookId, int employeeId) throws DaoException {
        Connection connection = null;
        PreparedStatement statement=null;
        try {
            connection = DbPool.getInstance().getConnection();
            statement = connection.prepareStatement(SQL_CHECK_BOOK_ID);
            statement.setInt(1,bookId);
            ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) {
                return false;
            }
            resultSet.close();
            statement.close();
            statement = connection.prepareStatement(SQL_CHECK_ID);
            statement.setInt(1,employeeId);
            resultSet = statement.executeQuery();
            if (!resultSet.next()) {
                return false;
            }
            resultSet.close();
            statement.close();
            statement = connection.prepareStatement(SQL_JOIN_TABLE_SELECT);
            statement.setInt(1,employeeId);
            resultSet = statement.executeQuery();
            boolean isPresent=false;
            while (resultSet.next()) {
                int readId = resultSet.getInt("BOOK_ID");
                if (readId==bookId) {
                    isPresent=true;
                    break;
                }
            }
            resultSet.close();
            statement.close();
            if (isPresent) {
                return false;
            }
            statement = connection.prepareStatement(SQL_JOIN_TABLE_INSERT);
            statement.setInt(1,bookId);
            statement.setInt(2,employeeId);
            statement.executeUpdate();
            return true;
        } catch (SQLException e) {
            throw new DaoException("Error while updating join table, bookId="+ String.valueOf(bookId)+" employeeId="+ String.valueOf(employeeId)+". "+e.getMessage());
        } finally {
            DbPool.getInstance().closeDbResources(connection,statement);
        }

    }
}
