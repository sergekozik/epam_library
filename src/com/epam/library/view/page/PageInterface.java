package com.epam.library.view.page;

import com.epam.library.entity.ViewRequestResponseBean;
import com.epam.library.view.exception.ViewException;

/**
 * Created by Serge_Kozik on 3/30/2017.
 */
public interface PageInterface {
    public ViewRequestResponseBean runPage(ViewRequestResponseBean response) throws ViewException;
}
