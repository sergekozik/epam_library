package com.epam.library.view.page;

import com.epam.library.entity.ViewRequestResponseBean;

/**
 * Created by Serge_Kozik on 4/3/2017.
 */
public interface ExceptionPageInterface {
    public ViewRequestResponseBean runPage(ViewRequestResponseBean response, Exception e);
}
