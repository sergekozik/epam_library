package com.epam.library.view.page.impl;

import com.epam.library.entity.BookEntity;
import com.epam.library.entity.EmployeeEntity;
import com.epam.library.entity.ViewRequestResponseBean;
import com.epam.library.view.console_printer.CustomConsolePrinter;
import com.epam.library.view.console_printer.impl.CustomConsolePrinterImpl;
import com.epam.library.view.exception.ViewException;
import com.epam.library.view.page.PageInterface;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Serge_Kozik on 3/30/2017.
 */
public class EmployeeListPage implements PageInterface {
    private final String PAGE_TITLE="List of all employees:";
    private final String TITLE_FIRST_COLUMN="Name:";
    private final String TITLE_SECOND_COLUMN="Birth Date:";
    private final String TITLE_THIRD_COLUMN="Email:";
    private final String PARAMETER_EMPLOYEES_LIST ="employees_list";
    private final String MENU_QUIT_KEY="q";
    private final String TITLE_MENU_QUIT ="Press ("+MENU_QUIT_KEY+") to return in main menu:";
    private final String LINK_MENU_MAIN="menu-main";

    private static CustomConsolePrinter consolePrinter = CustomConsolePrinterImpl.getInstance();

    private DateFormat dateFormatDDMMYYYY = new SimpleDateFormat("dd.MM.YYYY");

    @Override
    public ViewRequestResponseBean runPage(ViewRequestResponseBean response) throws ViewException {
        consolePrinter.printLine(PAGE_TITLE);
        List<String> names = new ArrayList<>();
        names.add(TITLE_FIRST_COLUMN);
        List<String> birthDates = new ArrayList<>();
        birthDates.add(TITLE_SECOND_COLUMN);
        List<String> emails = new ArrayList<>();
        emails.add(TITLE_THIRD_COLUMN);
        Object bookListObj = response.getAttribute(PARAMETER_EMPLOYEES_LIST);
        if (bookListObj==null) {
            throw new ViewException("Empty employees list");
        }
        List<EmployeeEntity> listEmployees = (List<EmployeeEntity>)bookListObj;

        for (EmployeeEntity person:listEmployees) {
            names.add(person.getName());
            Date birthDate = person.getDateOfBirth();
            birthDates.add(dateFormatDDMMYYYY.format(birthDate));
            emails.add(person.getEmail());
        }
        if (!consolePrinter.printThreeColumns(names,birthDates,emails)) {
            throw new ViewException("Error while employees list display.");
        }
        String resultLink = consolePrinter.confirmRedirect(TITLE_MENU_QUIT,MENU_QUIT_KEY,LINK_MENU_MAIN);
        ViewRequestResponseBean result = new ViewRequestResponseBean(resultLink);
        return result;
    }
}
