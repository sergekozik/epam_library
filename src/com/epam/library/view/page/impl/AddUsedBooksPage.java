package com.epam.library.view.page.impl;

import com.epam.library.entity.ViewRequestResponseBean;
import com.epam.library.view.console_printer.CustomConsolePrinter;
import com.epam.library.view.console_printer.impl.CustomConsolePrinterImpl;
import com.epam.library.view.exception.ViewException;
import com.epam.library.view.page.PageInterface;

/**
 * Created by Serge_Kozik on 3/30/2017.
 */
public class AddUsedBooksPage implements PageInterface {
    private static final String TITLE_ASK_USAGE_NUMBER = "Please input number of using books facts to generate:";
    private static final String LINK_DO_RANDOM_USAGE = "add-random-usages-do";
    private static final String PARAMETER_USAGE_NUMBER = "usages_number";

    private static CustomConsolePrinter consolePrinter = CustomConsolePrinterImpl.getInstance();

    @Override
    public ViewRequestResponseBean runPage(ViewRequestResponseBean response) throws ViewException {
        String numUsagesString = consolePrinter.askUserInput(TITLE_ASK_USAGE_NUMBER);
        int numUsages=0;
        try {
            numUsages = Integer.parseInt(numUsagesString);
        } catch (NumberFormatException e) {
            throw new ViewException(e);
        }
        ViewRequestResponseBean result = new ViewRequestResponseBean(LINK_DO_RANDOM_USAGE);
        result.setAttribute(PARAMETER_USAGE_NUMBER,numUsages);
        return result;
    }
}
