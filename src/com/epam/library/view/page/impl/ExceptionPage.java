package com.epam.library.view.page.impl;

import com.epam.library.entity.ViewRequestResponseBean;
import com.epam.library.view.console_printer.CustomConsolePrinter;
import com.epam.library.view.console_printer.impl.CustomConsolePrinterImpl;
import com.epam.library.view.page.ExceptionPageInterface;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Serge_Kozik on 4/3/2017.
 */
public class ExceptionPage implements ExceptionPageInterface {

    private static CustomConsolePrinter consolePrinter = CustomConsolePrinterImpl.getInstance();
    private final String MENU_QUIT_KEY="q";
    private final String TITLE_MENU_QUIT ="Press ("+MENU_QUIT_KEY+") to return in main menu:";
    private final String LINK_MENU_MAIN="menu-main";


    @Override
    public ViewRequestResponseBean runPage(ViewRequestResponseBean response, Exception e) {
        StringWriter sw = new StringWriter();
        e.printStackTrace(new PrintWriter(sw));
        consolePrinter.printLine(e.getMessage());
        consolePrinter.printLine(sw.toString());
        String resultLink = consolePrinter.confirmRedirect(TITLE_MENU_QUIT,MENU_QUIT_KEY,LINK_MENU_MAIN);
        ViewRequestResponseBean result = new ViewRequestResponseBean(resultLink);
        return result;
    }
}
