package com.epam.library.view.page.impl;

import com.epam.library.entity.ViewRequestResponseBean;
import com.epam.library.view.console_printer.CustomConsolePrinter;
import com.epam.library.view.console_printer.impl.CustomConsolePrinterImpl;
import com.epam.library.view.exception.ViewException;
import com.epam.library.view.page.PageInterface;

/**
 * Created by Serge_Kozik on 3/30/2017.
 */
public class AskBadReadersPage implements PageInterface {
    private static final String TITLE_ASK_BOOKS_NUMBER = "List of readers who read less than or equal to given books number. Please input number of books:";
    private static final String LINK_DO_BAD_READERS = "bad-readers-do";
    private static final String PARAMETER_BOOKS_NUMBER = "books_number";

    private static CustomConsolePrinter consolePrinter = CustomConsolePrinterImpl.getInstance();

    @Override
    public ViewRequestResponseBean runPage(ViewRequestResponseBean response) throws ViewException {
        String numBooksString = consolePrinter.askUserInput(TITLE_ASK_BOOKS_NUMBER);
        int numBooks=0;
        try {
            numBooks = Integer.parseInt(numBooksString);
        } catch (NumberFormatException e) {
            throw new ViewException(e);
        }
        ViewRequestResponseBean result = new ViewRequestResponseBean(LINK_DO_BAD_READERS);
        result.setAttribute(PARAMETER_BOOKS_NUMBER,numBooks);
        return result;
    }
}
