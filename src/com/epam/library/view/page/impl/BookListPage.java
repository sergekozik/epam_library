package com.epam.library.view.page.impl;

import com.epam.library.entity.BookEntity;
import com.epam.library.entity.ViewRequestResponseBean;
import com.epam.library.view.console_printer.CustomConsolePrinter;
import com.epam.library.view.console_printer.impl.CustomConsolePrinterImpl;
import com.epam.library.view.exception.ViewException;
import com.epam.library.view.page.PageInterface;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Serge_Kozik on 3/30/2017.
 */
public class BookListPage implements PageInterface {

    private final String PAGE_TITLE="List of all books:";
    private final String TITLE_FIRST_COLUMN="Books Title:";
    private final String TITLE_SECOND_COLUMN="Books Author:";
    private final String TITLE_THIRD_COLUMN="Publish Year:";
    private final String PARAMETER_BOOKS_LIST="books_list";
    private final String MENU_QUIT_KEY="q";
    private final String TITLE_MENU_QUIT ="Press ("+MENU_QUIT_KEY+") to return in main menu:";
    private final String LINK_MENU_MAIN="menu-main";

    private static CustomConsolePrinter consolePrinter = CustomConsolePrinterImpl.getInstance();

    @Override
    public ViewRequestResponseBean runPage(ViewRequestResponseBean response) throws ViewException {

        consolePrinter.printLine(PAGE_TITLE);
        List<String> titles = new ArrayList<>();
        titles.add(TITLE_FIRST_COLUMN);
        List<String> authors = new ArrayList<>();
        authors.add(TITLE_SECOND_COLUMN);
        List<String> years = new ArrayList<>();
        years.add(TITLE_THIRD_COLUMN);
        Object bookListObj = response.getAttribute(PARAMETER_BOOKS_LIST);
        if (bookListObj==null) {
            throw new ViewException("Empty books list");
        }
        List<BookEntity> listBooks = (List<BookEntity>)bookListObj;
        for (BookEntity book:listBooks) {
            titles.add(book.getTitle());
            authors.add(book.getAuthor());
            years.add(String.valueOf(book.getPublishYear()));
        }
        if (!consolePrinter.printThreeColumns(titles,authors,years)) {
            throw new ViewException("Error while book list display.");
        }
        String resultLink = consolePrinter.confirmRedirect(TITLE_MENU_QUIT,MENU_QUIT_KEY,LINK_MENU_MAIN);
        ViewRequestResponseBean result = new ViewRequestResponseBean(resultLink);
        return result;
    }
}
