package com.epam.library.view.page.impl;

import com.epam.library.entity.ViewRequestResponseBean;
import com.epam.library.view.console_printer.CustomConsolePrinter;
import com.epam.library.view.console_printer.impl.CustomConsolePrinterImpl;
import com.epam.library.view.page.PageInterface;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by Serge_Kozik on 3/30/2017.
 */
public class MainMenuPage implements PageInterface {
    private final String TITLE_MENU_ADD_RANDOM_BOOKS = "Add random books to database.";
    private final String LINK_MENU_ADD_RANDOM_BOOKS = "add-random-books";
    private final String KEY_MENU_ADD_RANDOM_BOOKS = "r";
    private final String TITLE_MENU_ADD_RANDOM_USAGE = "Add random records for books usage to database.";
    private final String LINK_MENU_ADD_RANDOM_USAGE = "add-random-usage";
    private final String KEY_MENU_ADD_RANDOM_USAGE = "u";
    private final String TITLE_MENU_ASK_BAD_READERS = "Print employees who have read less than or equal to given books quantity.";
    private final String LINK_MENU_ASK_BAD_READERS = "ask-bad-readers";
    private final String KEY_MENU_ASK_BAD_READERS = "b";
    private final String TITLE_MENU_ASK_GOOD_READERS = "Print employees who have read more than or equal to given books quantity.";
    private final String LINK_MENU_ASK_GOOD_READERS = "ask-good-readers";
    private final String KEY_MENU_ASK_GOOD_READERS = "g";
    private final String TITLE_MENU_LIST_BOOKS = "Print all books.";
    private final String LINK_MENU_LIST_BOOKS = "list-all-books";
    private final String KEY_MENU_LIST_BOOKS = "o";
    private final String TITLE_MENU_LIST_EMPLOYEES = "Print all employees.";
    private final String LINK_MENU_LIST_EMPLOYEES = "list-all-employees";
    private final String KEY_MENU_LIST_EMPLOYEES = "e";



    private static CustomConsolePrinter consolePrinter = CustomConsolePrinterImpl.getInstance();

    @Override
    public ViewRequestResponseBean runPage(ViewRequestResponseBean response) {
        List<String> menuTitles = new ArrayList<>();
        List<String> menuLinks =new ArrayList<>();
        List<String> menuKeys =new ArrayList<>();

        menuTitles.add(TITLE_MENU_ADD_RANDOM_BOOKS);
        menuLinks.add(LINK_MENU_ADD_RANDOM_BOOKS);
        menuKeys.add(KEY_MENU_ADD_RANDOM_BOOKS);

        menuTitles.add(TITLE_MENU_ADD_RANDOM_USAGE);
        menuLinks.add(LINK_MENU_ADD_RANDOM_USAGE);
        menuKeys.add(KEY_MENU_ADD_RANDOM_USAGE);

        menuTitles.add(TITLE_MENU_ASK_BAD_READERS);
        menuLinks.add(LINK_MENU_ASK_BAD_READERS);
        menuKeys.add(KEY_MENU_ASK_BAD_READERS);

        menuTitles.add(TITLE_MENU_ASK_GOOD_READERS);
        menuLinks.add(LINK_MENU_ASK_GOOD_READERS);
        menuKeys.add(KEY_MENU_ASK_GOOD_READERS);

        menuTitles.add(TITLE_MENU_LIST_BOOKS);
        menuLinks.add(LINK_MENU_LIST_BOOKS);
        menuKeys.add(KEY_MENU_LIST_BOOKS);

        menuTitles.add(TITLE_MENU_LIST_EMPLOYEES);
        menuTitles.add(LINK_MENU_LIST_EMPLOYEES);
        menuKeys.add(KEY_MENU_LIST_EMPLOYEES);

        String userLink = consolePrinter.printMenu(menuTitles,menuLinks,menuKeys);
        return new ViewRequestResponseBean(userLink);
    }
}
