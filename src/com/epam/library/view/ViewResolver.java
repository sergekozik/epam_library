package com.epam.library.view;

import com.epam.library.entity.ViewRequestResponseBean;

/**
 * Created by Serge_Kozik on 3/30/2017.
 */
public interface ViewResolver {
    ViewRequestResponseBean passToView(ViewRequestResponseBean response);

}
