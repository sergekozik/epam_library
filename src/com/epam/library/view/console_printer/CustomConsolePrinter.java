package com.epam.library.view.console_printer;

import java.util.List;

/**
 * Created by Serge on 20.03.2017.
 */
public interface CustomConsolePrinter {
    public boolean printTwoColumns(List<String> firstColumn,List<String> secondColumn);
    public boolean printThreeColumns(List<String> firstColumn,List<String> secondColumn,List<String> thirdColumn);
    public boolean printTwoColumnsReport(List<String> firstColumn,List<String> secondColumn);
    public boolean printThreeColumnsReport(List<String> firstColumn,List<String> secondColumn,List<String> thirdColumn);
    public boolean printLine(String line);
    public String printMenu(List<String> menuTitles, List<String> menuLinks, List<String> menuKeyShortcuts);
    public String askUserInput(String question);
    public String confirmRedirect(String warningMessage, String keyOK, String link);
}
