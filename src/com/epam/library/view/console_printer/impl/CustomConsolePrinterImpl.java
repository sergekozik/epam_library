package com.epam.library.view.console_printer.impl;

import com.epam.library.view.console_printer.CustomConsolePrinter;

import java.util.List;
import java.util.Scanner;

/**
 * Created by Serge on 20.03.2017.
 */
public class CustomConsolePrinterImpl implements CustomConsolePrinter {

    private static CustomConsolePrinter instance = new CustomConsolePrinterImpl();
    private static Scanner scanner = new Scanner(System.in);

    private CustomConsolePrinterImpl() {
    }

    public static CustomConsolePrinter getInstance() {
        return instance;
    }

    @Override
    public boolean printTwoColumns(List<String> firstColumn, List<String> secondColumn) {
        if ((firstColumn.size()==secondColumn.size())) {
            for (int ii=0; ii<firstColumn.size(); ii++) {
                System.out.println(firstColumn.get(ii)+" "+secondColumn.get(ii));
            }
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean printThreeColumns(List<String> firstColumn, List<String> secondColumn, List<String> thirdColumn) {
        if ((firstColumn.size()==secondColumn.size())&&(firstColumn.size()==thirdColumn.size())) {
            for (int ii=0; ii<firstColumn.size(); ii++) {
                System.out.println(firstColumn.get(ii)+", "+secondColumn.get(ii)+": "+thirdColumn.get(ii));
            }
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean printTwoColumnsReport(List<String> firstColumn, List<String> secondColumn) {
        if ((firstColumn.size()==secondColumn.size())) {
            for (int ii=0; ii<firstColumn.size(); ii++) {
                System.out.println(firstColumn.get(ii)+": "+secondColumn.get(ii));
            }
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean printThreeColumnsReport(List<String> firstColumn, List<String> secondColumn, List<String> thirdColumn) {
        if ((firstColumn.size()==secondColumn.size())&&(firstColumn.size()==thirdColumn.size())) {
            for (int ii=0; ii<firstColumn.size(); ii++) {
                System.out.println(firstColumn.get(ii)+" "+secondColumn.get(ii)+" "+thirdColumn.get(ii));
            }
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean printLine(String line) {
        System.out.println(line);
        return true;
    }

    @Override
    public String printMenu(List<String> menuTitles, List<String> menuLinks, List<String> menuKeyShortcuts) {
        if ((menuTitles.size()==menuLinks.size())&&(menuTitles.size()==menuKeyShortcuts.size())) {
            String userInput;
            do {
                for (int ii=0; ii<menuTitles.size(); ii++) {
                    System.out.println(menuTitles.get(ii)+"("+menuKeyShortcuts.get(ii)+");");
                }
                userInput=scanner.nextLine().trim();
            } while (menuKeyShortcuts.contains(userInput));
            return menuLinks.get(menuKeyShortcuts.indexOf(userInput));
        } else {
            return null;
        }
    }

    @Override
    public String askUserInput(String question) {
        System.out.println(question);
        return scanner.nextLine().trim();
    }

    @Override
    public String confirmRedirect(String warningMessage, String keyOK, String link) {
        String userInput;
        do {
            System.out.println(warningMessage);
            userInput=scanner.nextLine().trim();
        } while (keyOK.equals(userInput));
        return link;
    }
}
