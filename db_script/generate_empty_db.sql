SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema epam_library
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `epam_library` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `epam_library` ;

-- -----------------------------------------------------
-- Table `epam_library`.`BOOK`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `epam_library`.`BOOK` (
  `ID` INT NOT NULL AUTO_INCREMENT,
  `BRIEF` VARCHAR(50) NULL,
  `PUBLISH_YEAR` INT NULL,
  `AUTHOR` VARCHAR(255) NULL,
  PRIMARY KEY (`ID`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `epam_library`.`EMPLOYEE`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `epam_library`.`EMPLOYEE` (
  `ID` INT NOT NULL AUTO_INCREMENT,
  `NAME` VARCHAR(255) NULL,
  `DATE_OF_BIRTH` DATE NULL,
  `EMAIL` VARCHAR(50) NULL,
  PRIMARY KEY (`ID`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `epam_library`.`EMPLOYEE_BOOK`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `epam_library`.`EMPLOYEE_BOOK` (
  `BOOK_ID` INT NOT NULL,
  `EMPLOYEE_ID` INT NOT NULL,
  `ID` INT NULL AUTO_INCREMENT,
  PRIMARY KEY (`BOOK_ID`, `EMPLOYEE_ID`),
  INDEX `fk_BOOK_has_EMPLOYEE_EMPLOYEE1_idx` (`EMPLOYEE_ID` ASC),
  INDEX `fk_BOOK_has_EMPLOYEE_BOOK_idx` (`BOOK_ID` ASC),
  UNIQUE INDEX `ID_UNIQUE` (`ID` ASC),
  CONSTRAINT `fk_BOOK_has_EMPLOYEE_BOOK`
    FOREIGN KEY (`BOOK_ID`)
    REFERENCES `epam_library`.`BOOK` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_BOOK_has_EMPLOYEE_EMPLOYEE1`
    FOREIGN KEY (`EMPLOYEE_ID`)
    REFERENCES `epam_library`.`EMPLOYEE` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
